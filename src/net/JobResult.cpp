/* XMRig
 * Copyright 2010      Jeff Garzik <jgarzik@pobox.com>
 * Copyright 2012-2014 pooler      <pooler@litecoinpool.org>
 * Copyright 2014      Lucas Jones <https://github.com/lucasjones>
 * Copyright 2014-2016 Wolf9466    <https://github.com/OhGodAPet>
 * Copyright 2016      Jay D Dee   <jayddee246@gmail.com>
 * Copyright 2017-2018 XMR-Stak    <https://github.com/fireice-uk>, <https://github.com/psychocrypt>
 * Copyright 2018-2020 SChernykh   <https://github.com/SChernykh>
 * Copyright 2016-2020 XMRig       <https://github.com/xmrig>, <support@xmrig.com>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <cstdio>

#include <iostream>
#include "base/net/stratum/Job.h"
#include "base/tools/Buffer.h"
#include "net/JobResult.h"
#include "3rdparty/uint256_t/uint256_t.h"

xmrig::JobResult::JobResult(int64_t id, const char *jobId, const uint64_t *nonce, const char *result, const xmrig::Algorithm &algorithm, const uint64_t *height) :

    algorithm(algorithm),
    nonce(nonce),
    result(result),
    id(id),
    jobId(jobId),
    diff(0),
    height(height),
    m_actualDiff(0)



{

    if (result && strlen(result) == 64) {
        uint64_t target = 0;
        uint8_t target2[32]{ 0 };

        Buffer::fromHex(result + 48, 16, reinterpret_cast<unsigned char*>(&target));
        Buffer::fromHex(result, 64, target2);

        if (target > 0) {


                uint256_t foo = (uint256_t)target2[0] << 8*31 |
                    (uint256_t)target2[1] << 8*30 |
                    (uint256_t)target2[2] << 8*29 |
                    (uint256_t)target2[3] << 8*28 |
                    (uint256_t)target2[4] << 8*27 |
                    (uint256_t)target2[5] << 8*26 |
                    (uint256_t)target2[6] << 8*25 |
                    (uint256_t)target2[7] << 8*24 |
                    (uint256_t)target2[8] << 8*23 |
                    (uint256_t)target2[9] << 8*22 |
                    (uint256_t)target2[10] << 8*21 |
                    (uint256_t)target2[11] << 8*20 |
                    (uint256_t)target2[12] << 8*19 |
                    (uint256_t)target2[13] << 8*18 |
                    (uint256_t)target2[14] << 8*17 |
                    (uint256_t)target2[15] << 8*16 |
                    (uint256_t)target2[16] << 8*15 |
                    (uint256_t)target2[17] << 8*14 |
                    (uint256_t)target2[18] << 8*13 |
                    (uint256_t)target2[19] << 8*12 |
                    (uint256_t)target2[20] << 8*11 |
                    (uint256_t)target2[21] << 8*10 |
                    (uint256_t)target2[22] << 8*9 |
                    (uint256_t)target2[23] << 8*8 |
                    (uint256_t)target2[24] << 8*7 |
                    (uint256_t)target2[25] << 8*6 |
                    (uint256_t)target2[26] << 8*5 |
                    (uint256_t)target2[27] << 8*4 |
                    (uint256_t)target2[28] << 8*3 |
                    (uint256_t)target2[29] << 8*2 |
                    (uint256_t)target2[30] << 8*1 |
                    (uint256_t)target2[31] << 8*0;


          uint256_t newtarget = uint256_max / foo;
          m_actualDiff = static_cast<uint64_t>(newtarget);


        }
    }
}


bool xmrig::JobResult::isCompatible(uint8_t fixedByte) const
{

    return false;
    /*uint8_t n[4];
    if (!Buffer::fromHex(nonce, 8, n)) {
        return false;
    }

    return n[3] == fixedByte;*/
}


bool xmrig::JobResult::isValid() const
{


    if (!nonce || m_actualDiff == 0) {
        return false;
    }
  return !jobId.isNull();
}
